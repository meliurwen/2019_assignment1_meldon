#!/usr/bin/env python

import mysql.connector
from mysql.connector import errorcode
import datetime

def retCursor():
    db = mysql.connector.connect(host='173.249.32.7', password='unibicoccadisco',  user='root')
    cursor = db.cursor()
    cursor.execute("USE website;")
    return cursor, db

class Database:
    
    def initializeDB():
        try:
         	#open database connection
            cursor, db = retCursor()

         	#if database `website` doesn't exist we'll create it
            cursor.execute("CREATE DATABASE IF NOT EXISTS website;")
            cursor.execute("USE website;")

         	#if tables doesn't exists, create and open it
            cursor.execute("CREATE TABLE IF NOT EXISTS visitors(ip VARCHAR(255) \
                            primary key, lastDate DATE, visit INT);")

            db.commit()		
            db.close()

            print('Creazione database andata a buon fine')
            return True
        except:
            return False

    
    def updateVisit(ip):
        cursor, db = retCursor()
        try:
            query = """INSERT INTO visitors(ip,lastDate,visit) VALUES(%s,NOW(),1) 
                       ON DUPLICATE KEY UPDATE lastDate=NOW(), visit = visit +1;"""
            cursor.execute(query, (ip,))
            db.commit()
            db.close()
            return {"success":True, "description":"All good"}
        except Exception as error:
            return {"success":False, "description":error}

    @staticmethod
    def lastVisit(ip):
        cursor, db = retCursor()
        try:
            query = """SELECT lastDate,visit FROM website.visitors WHERE ip = %s;"""
            cursor.execute(query, (ip,))
            tmp_list = []
            row = cursor.fetchone()
            while row is not None:
                for tmp in row:
                    tmp_list.append(tmp)
                row = cursor.fetchone()
            db.close()
            print(tmp_list)
            if not tmp_list:
                return {"success":True, "description":"Not found"}
            else:
                return {"success":True, "description":"Found",
                        "lastDate":tmp_list[0], "visit":tmp_list[1]}
        except Exception as error:
            return {"success":False, "description":error}
