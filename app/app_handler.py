#!/usr/bin/env python

from flask import Flask, escape, request, jsonify
from app.database import Database

# connecting to the database
Database.initializeDB()


app = Flask(__name__)

@app.route('/')
def hello():
    # In case this app is hosted behind a reverse proxy
    ipAddr = request.environ.get('HTTP_X_REAL_IP',
                                 request.remote_addr)
    if not ipAddr:
        ipAddr = request.remote_addr

    data = Database.lastVisit(ipAddr)

    if data["success"]:
        if data["description"] == "Found":
            message = "<h1>Hi!</h1></br>Your IPv4 address is <b>" + ipAddr + \
                      "</b></br>Your last visit was at <b>" + str(data["lastDate"]) + \
                      "</b></br>For a total of <b>" + str(int(data["visit"])+1) + "</b> times! :D"
        else:
            message = "<h1>Welcome!</h1></br><h2>This is your first visit! :D</h2> \
                       </br>Your IPv4 address is <b>" + ipAddr + "</b>"
    else:
        print(data["description"])
        return "<h1>Error 500</h1></br>An error has occurred on reading the database! :(", 500
    visitUpdated = Database.updateVisit(ipAddr)
    print(visitUpdated)
    if not visitUpdated["success"]:
        return "<h1>Error 500</h1></br>An error has occurred during the \
registration of your visit! :(", 500

    return message

