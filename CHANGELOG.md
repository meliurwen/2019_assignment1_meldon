## Version 1.2.34

#### New feature
+ Now works behind a reverse-proxy like `nginx` or `apache`
+ Added `Dockerfile` and `docker-compose.yml` files
+ Updated `README.md`

#### Bug fix
+ Fixed issue that was making Flask to listen only 127.0.0.1

## Version 1.1.33

#### New freature
+ Nessuna

#### Bug fix
+ dimenticanze nel readme

## Version 1.1.32

#### New freature
+ Cambio di rotta, si fa un'altra applicazione, questa volta seria

#### Bug fix
+ troppi

## Version 1.0.30

#### Bug fix
+ readme logo image

## Version 1.0.29

#### New freature
+ Aggunti file per semplificazione della pipeline

#### Bug fix
+ varie corezzioni

## Version 1.0.28

#### New freature
+ edit 1
+ edit 2
+ edit 3

#### Bugfix
+ edit 1
+ edit 2
+ edit 3

## Version 1.0.0

#### Test changelog

+ edit 1
+ edit 2
+ edit 3


## Version 0.1.0

#### Test changelog

+ edit 1
+ edit 2
+ edit 3

