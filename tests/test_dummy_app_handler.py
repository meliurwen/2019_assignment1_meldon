import unittest
import requests
from mock import Mock, patch
from app.database import Database

# Tests for `app_handler.py`

class DemoTestCase(unittest.TestCase):

    def test_create_database(self):
        # Does this function actually return True?
        print('Creating database')
        self.assertTrue(Database.initializeDB())

    def test_updateVisit(self):
        # Does this function actually return False?
        self.assertTrue(Database.updateVisit("192.168.1.1")["success"])

    def test_lastVisit(self):
        # Does this function actually return True?
        self.assertTrue(Database.lastVisit("192.168.1.1")["success"])

if __name__ == '__main__':
    unittest.main()
