<div align="center">

<img src="img/meldon_logo.png" alt="MelDon Logo" width="200" >

# MelDon

[![pipeline status](https://gitlab.com/Territory91/2019_assignment1_meldon/badges/master/pipeline.svg)](https://gitlab.com/Territory91/2019_assignment1_meldon/commits/master) [![coverage report](https://gitlab.com/Territory91/2019_assignment1_meldon/badges/master/coverage.svg)](https://gitlab.com/Territory91/2019_assignment1_meldon/commits/master) [![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://gitlab.com/Territory91/2019_assignment1_meldon/blob/master/LICENSE) [![Pylint](https://gitlab.com/Territory91/2019_assignment1_meldon/-/jobs/artifacts/master/raw/pylint/pylint.svg?job=pylint)](https://gitlab.com/Territory91/2019_assignment1_meldon/-/jobs/artifacts/master/raw/pylint/pylint.log?job=pylint) 

</div>

# 2019 Assignment 1

> **_Note:_** It's a private repo, use the `sw_dev_process_course` user in order to access it!

+ **_Project's Link:_** https://gitlab.com/Territory91/2019_assignment1_meldon
+ **_Prototype of this repo:_** https://gitlab.com/Territory91/test-python
+ **_Demo Link:_** https://demo.eracolatore.tk

## Purpose

Learn how to automate the entire development process setting up a CI/CD pipeline.

In this project we'll use the Gitlab's CI/CD infrastructure.

## App

> **Note:** At the moment it's in pre-alpha state; a sensible part of our available resources has been invested on the study of CI/CD and Python's Packaging best practices.

A simple web app living in the cloud with bare bones functionalities!  ☁️🗒

It's composed by two core and one optional components:

+ 💽 **Database** (_MySQL_)
+ 🌐 **WebApp**

Our app simply registers a user's last date visit and the number of accesses to a website.

For simplicity resons the user is identified by its IPv4 address; no cookies or fingerprinting techniques are applied.

## The Pipeline

> **Note:** We also have `Dockerfile` and `docker-compose.yml` files, but we have decided to not include them in the pipeline. We created them only to self-host the [demo](https://demo.eracolatore.tk) in our VPS. The `docker-compose.yml` we are using on our VPS has some more parameters (with sensible data) in order to make it work behind our reverse-proxy.

At the moment the pipeline is composed by **5** stages and **2** branches:

**Stages:**

+ **Build**
+ **Verify**
  + falke8
  + pylama
  + pylint
+ **Test**
  + unittest
+ **Packaging**
  + *pip Wheel* `.whl` Package
  + *Debian 10* `.deb` Package
  + *Ubuntu 18.04* `.deb` Package
+ **Release** with ***tags*** ([see here for the released packages](https://gitlab.com/Territory91/2019_assignment1_meldon/-/releases))

![](img/release.png)

**Branches:**

+ **master** (the whole pipeline)
+ **develop** (the first 4 stages)

![meldon_logo](img/pipeline.png)

## Build from Source

### Debian and derivatives

Install the dependencies:

```sh
apt-get -y install python3 python3-venv python3-pip fakeroot \
                   python3-all debhelper python-all \
                   python3-setuptools build-essential
```

Enter the project's folder, _create_ the **_virtual environment_** and then _activate_ it:

```sh
cd 2019_assignment1_MelDon
```

```sh
python3 -m venv .
```

```sh
source bin/activate
```

Install some other dependencies from **_pip_**:
> **Note:** `wheel` must be installed separately because `stdeb` installation process depends on it.

```sh
pip3 install wheel && pip3 install stdeb setuptools
```

Now generate the deb package:

```sh
python3 setup.py --command-packages=stdeb.command bdist_deb
```

### pip

Simply type:

```sh
python3 setup.py install
```

## Group Members

+ Salanti Michele - 793891
+ Donati Ivan - 781022
